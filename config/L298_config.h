/**
 * @file      L298_config.h
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-Quatro (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _L298_CONFIG_H_
#define	_L298_CONFIG_H_

#define L298_0_POL 1
#define L298_1_POL 1
#define L298_2_POL 0
#define L298_3_POL 0

#define L298_SETUP_ANSEL() do{ANCON0bits.ANSEL4 = 0;}while(0)

#define L298_0_IN1_TRIS TRISC0
#define L298_0_IN2_TRIS TRISA5
#define L298_1_IN1_TRIS TRISA6
#define L298_1_IN2_TRIS TRISA7
#define L298_2_IN1_TRIS TRISD0
#define L298_2_IN2_TRIS TRISD1
#define L298_3_IN1_TRIS TRISD2
#define L298_3_IN2_TRIS TRISD3

#define L298_0_IN1_LAT  LATC0
#define L298_0_IN2_LAT  LATA5
#define L298_1_IN1_LAT  LATA6
#define L298_1_IN2_LAT  LATA7
#define L298_2_IN1_LAT  LATD0
#define L298_2_IN2_LAT  LATD1
#define L298_3_IN1_LAT  LATD2
#define L298_3_IN2_LAT  LATD3

#endif	/* _L298_CONFIG_H_ */

