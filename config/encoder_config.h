/**
 * @file      encoder_config.h
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-Quatro (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _ENCODER_CONFIG_H_
#define	_ENCODER_CONFIG_H_

#include <xc.h>

#define ENCODER_PORT PORTA

#define ENCODER_0_DIR -1
#define ENCODER_1_DIR -1
#define ENCODER_2_DIR 1
#define ENCODER_3_DIR 1

#define ENCODER_0_A_INTIE INT0IE
#define ENCODER_0_A_INTIF INT0IF
#define ENCODER_0_A_INTIP(x) do{}while(0)
#define ENCODER_0_A 0x01U
#define ENCODER_0_B 0x01U
#define ENCODER_1_A_INTIE INT1IE
#define ENCODER_1_A_INTIF INT1IF
#define ENCODER_1_A_INTIP(x) do{INT1IP=x;}while(0)
#define ENCODER_1_A 0x02U
#define ENCODER_1_B 0x02U
#define ENCODER_2_A_INTIE INT2IE
#define ENCODER_2_A_INTIF INT2IF
#define ENCODER_2_A_INTIP(x) do{INT2IP=x;}while(0)
#define ENCODER_2_A 0x04U
#define ENCODER_2_B 0x04U
#define ENCODER_3_A_INTIE INT3IE
#define ENCODER_3_A_INTIF INT3IF
#define ENCODER_3_A_INTIP(x) do{INT3IP=x;}while(0)
#define ENCODER_3_A 0x08U
#define ENCODER_3_B 0x08U

#define ENCODER_SETUP_ANSEL() do{ANCON0 &= ~0b00001111; ANCON1 &= ~0b00000101;}while(0)


#endif	/* _ENCODER_CONFIG_H_ */

