/**
 * @file      pwm_config.h
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-Quatro (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _PWM_CONFIG_H_
#define _PWM_CONFIG_H_

#include <xc.h>

#define PWM_SETUP_ANSEL() do{}while(0)
#define PWM_SETUP_TRIS() do{TRISC&=~0b11000100;TRISD&=~0b00010000;}while(0)

#define PWM_0_CCP_H  CCPR1L
#define PWM_0_CCP_B1 DC1B1
#define PWM_0_CCP_B0 DC1B0

#define PWM_1_CCP_H  CCPR2L
#define PWM_1_CCP_B1 DC2B1
#define PWM_1_CCP_B0 DC2B0

#define PWM_2_CCP_H  CCPR3L
#define PWM_2_CCP_B1 DC3B1
#define PWM_2_CCP_B0 DC3B0

#define PWM_3_CCP_H  CCPR4L
#define PWM_3_CCP_B1 DC4B1
#define PWM_3_CCP_B0 DC4B0

#endif	/* _PWM_CONFIG_H_ */

