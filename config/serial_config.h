/**
 * @file      serial_config.h
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-Quatro (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _SERIAL_CONFIG_H_
#define	_SERIAL_CONFIG_H_

#include <xc.h>

#define SERIAL_SETUP_ANSEL() do{}while(0)
#define SERIAL_TX_TRIS TRISDbits.TRISD6
#define SERIAL_RX_TRIS TRISDbits.TRISD7

#define SERIAL_TXSTA TXSTA2
#define SERIAL_RCSTA RCSTA2
#define SERIAL_BAUDCON BAUDCON2
#define SERIAL_SPBRG SPBRG2
#define SERIAL_TXIP TX2IP
#define SERIAL_TXIF TX2IF
#define SERIAL_TXIE TX2IE
#define SERIAL_RCIP RC2IP
#define SERIAL_RCIF RC2IF
#define SERIAL_RCIE RC2IE
#define SERIAL_RCREG RCREG2
#define SERIAL_TXREG TXREG2

#endif	/* _SERIAL_CONFIG_H_ */
