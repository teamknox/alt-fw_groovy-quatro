/**
 * @file      motors_control.c
 * @author    2020 Shoji Yamamoto
 * ===============================================================
 * ALT-Groovy-Quatro (Version 1.0.1)
 * Copyright (c) 2020 Shoji Yamamoto
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2020 Shoji Yamamoto
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <xc.h>
#include "led.h"
#include "encoder.h"
#include "L298.h"
#include "servo.h"
#include "sys_clock.h"
#include "parameter.h"
#include "motors_control.h"

static ServoWorking position_loop[MOTORS_CONTROL_MOTOR_COUNT];
static ControlMode motor_mode[MOTORS_CONTROL_MOTOR_COUNT];
static int16_t motor_target[MOTORS_CONTROL_MOTOR_COUNT];
static ControlMode isr_motor_mode[MOTORS_CONTROL_MOTOR_COUNT];
static ControlMode isr_motor_mode_last[MOTORS_CONTROL_MOTOR_COUNT];
static int16_t isr_motor_target[MOTORS_CONTROL_MOTOR_COUNT];
static int16_t isr_motor_actual_position[MOTORS_CONTROL_MOTOR_COUNT];
static int16_t isr_motor_demand_position[MOTORS_CONTROL_MOTOR_COUNT];
static int16_t isr_motor_demand_pwm[MOTORS_CONTROL_MOTOR_COUNT];
static uint8_t isr_motor_new_command_flags[1 + (MOTORS_CONTROL_MOTOR_COUNT / 8)];
static uint16_t isr_motor_last_command_cycle[MOTORS_CONTROL_MOTOR_COUNT];
static ControlMode motor_mode_last[MOTORS_CONTROL_MOTOR_COUNT];
static int16_t motor_actual_position[MOTORS_CONTROL_MOTOR_COUNT];
static int16_t motor_demand_position[MOTORS_CONTROL_MOTOR_COUNT];
static int16_t motor_demand_pwm[MOTORS_CONTROL_MOTOR_COUNT];
static uint8_t motor_new_command_flags[1 + (MOTORS_CONTROL_MOTOR_COUNT / 8)];
static uint16_t motor_last_command_cycle[MOTORS_CONTROL_MOTOR_COUNT];

static uint16_t motors_control_isr_event_counter = 0;
static int8_t motors_control_isr_event_updated = 1;

#define POS_FIXED_POINT_SIZE 16
#define DELTA_T MOTORS_CONTROL_DELTA_T
#define POS_KP(Gain)  (int32_t)((Gain) * (float)(1ULL << POS_FIXED_POINT_SIZE))
#define POS_KI(Ti)  (int32_t)((DELTA_T * (float)(1ULL << POS_FIXED_POINT_SIZE)) / (Ti))

static ServoParameter position_loop_parameter = {
    .FixedPointSize = POS_FIXED_POINT_SIZE,
    .UniDirectional = 0,
    .ErrorMin = PARAMETER_ERROR_MIN_DEFAULT_VALUE,
    .ErrorMax = PARAMETER_ERROR_MAX_DEFAULT_VALUE,
    .IgnoreMin = PARAMETER_IGNORE_MIN_DEFAULT_VALUE,
    .IgnoreMax = PARAMETER_IGNORE_MAX_DEFAULT_VALUE,
    .MV_HysteresisMin = ((int32_t)PARAMETER_HYSTERESIS_MIN_DEFAULT_VALUE) << POS_FIXED_POINT_SIZE,
    .MV_HysteresisMax = ((int32_t)PARAMETER_HYSTERESIS_MAX_DEFAULT_VALUE) << POS_FIXED_POINT_SIZE,
    .MV_Min = ((int32_t)PARAMETER_MV_MIN_DEFAULT_VALUE) << POS_FIXED_POINT_SIZE,
    .MV_Max = ((int32_t)PARAMETER_MV_MAX_DEFAULT_VALUE) << POS_FIXED_POINT_SIZE,
    .Kp = POS_KP(PARAMETER_GAIN_P_DEFAULT_VALUE),
    .Ki = POS_KI(PARAMETER_GAIN_Ti_DEFAULT_VALUE)
};

void motors_control_set_parameter_error_min(int16_t min)
{
    TMR4IE = 0;
    position_loop_parameter.ErrorMin = min;
    TMR4IE = 1;
}

void motors_control_set_parameter_error_max(int16_t max)
{
    TMR4IE = 0;
    position_loop_parameter.ErrorMax = max;
    TMR4IE = 1;
}

int16_t motors_control_get_parameter_error_min(void)
{
    return position_loop_parameter.ErrorMin;
}

int16_t motors_control_get_parameter_error_max(void)
{
    return position_loop_parameter.ErrorMax;
}

void motors_control_set_parameter_ignore_min(int16_t min)
{
    TMR4IE = 0;
    position_loop_parameter.IgnoreMin = min;
    TMR4IE = 1;
}

void motors_control_set_parameter_ignore_max(int16_t max)
{
    TMR4IE = 0;
    position_loop_parameter.IgnoreMax = max;
    TMR4IE = 1;
}

int16_t motors_control_get_parameter_ignore_min(void)
{
    return position_loop_parameter.IgnoreMin;
}

int16_t motors_control_get_parameter_ignore_max(void)
{
    return position_loop_parameter.IgnoreMax;
}

void motors_control_set_parameter_hysteresis_min(int16_t min)
{
    int32_t min32 = (int32_t)min;
    min32 <<= POS_FIXED_POINT_SIZE;
    TMR4IE = 0;
    position_loop_parameter.MV_HysteresisMin = min32;
    TMR4IE = 1;
}

void motors_control_set_parameter_hysteresis_max(int16_t max)
{
    int32_t max32 = (int32_t)max;
    max32 <<= POS_FIXED_POINT_SIZE;
    TMR4IE = 0;
    position_loop_parameter.MV_HysteresisMax = max32;
    TMR4IE = 1;
}

int16_t motors_control_get_parameter_hysteresis_min(void)
{
    return (int16_t)(position_loop_parameter.MV_HysteresisMin >> POS_FIXED_POINT_SIZE);
}

int16_t motors_control_get_parameter_hysteresis_max(void)
{
    return (int16_t)(position_loop_parameter.MV_HysteresisMax >> POS_FIXED_POINT_SIZE);
}

void motors_control_set_parameter_mv_min(int16_t min)
{
    int32_t min32 = (int32_t)min;
    min32 <<= POS_FIXED_POINT_SIZE;
    TMR4IE = 0;
    position_loop_parameter.MV_Min = min32;
    TMR4IE = 1;
}

void motors_control_set_parameter_mv_max(int16_t max)
{
    int32_t max32 = (int32_t)max;
    max32 <<= POS_FIXED_POINT_SIZE;
    TMR4IE = 0;
    position_loop_parameter.MV_Max = max32;
    TMR4IE = 1;
}

int16_t motors_control_get_parameter_mv_min(void)
{
    return (int16_t)(position_loop_parameter.MV_Min >> POS_FIXED_POINT_SIZE);
}

int16_t motors_control_get_parameter_mv_max(void)
{
    return (int16_t)(position_loop_parameter.MV_Max >> POS_FIXED_POINT_SIZE);
}

void motors_control_set_parameter_gain_P(float Gp)
{
    int32_t Kp32 = POS_KP(Gp);
    TMR4IE = 0;
    position_loop_parameter.Kp = Kp32;
    TMR4IE = 1;
}

void motors_control_set_parameter_gain_Ti(float Ti)
{
    int32_t Ti32 = POS_KI(Ti);
    TMR4IE = 0;
    position_loop_parameter.Ki = Ti32;
    TMR4IE = 1;
}

float motors_control_get_parameter_gain_P(void)
{
    return (float)position_loop_parameter.Kp / (float)(1ULL << POS_FIXED_POINT_SIZE);
}

float motors_control_get_parameter_gain_Ti(void)
{
    return (DELTA_T * (float)(1ULL << POS_FIXED_POINT_SIZE)) / (float)position_loop_parameter.Ki;
}

void motors_control_init(void) {
    L298_init();
    int i;
    for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
        motor_mode[i] = MOTORS_CONTROL_MODE_NONE;
        motor_target[i] = 0;
        servo_init(&position_loop[i]);
        position_loop[i].Parameter = &position_loop_parameter;
        motor_actual_position[i] = isr_motor_actual_position[i] = 0;
        motor_demand_position[i] = isr_motor_demand_position[i] = 0;
        motor_demand_pwm[i] = isr_motor_demand_pwm[i] = 0;
        isr_motor_new_command_flags[i / 8] = motor_new_command_flags[i / 8] = 0;
    }
    encoder_init();
    T4CON = MOTORS_CONTROL_T4CON;
    TMR4 = 0;
    PR4 = 0xFF;
    TMR4IP = 0;
    TMR4IF = 0;
    TMR4IE = 1;
    for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
        motors_control_set_command(i, MOTORS_CONTROL_MODE_BRK, 1023);
    }
    TMR4ON = 1;
}

void motors_control_set_command(int8_t idx, ControlMode mode, int16_t target) {
    if (idx < MOTORS_CONTROL_MOTOR_COUNT) {
        motor_mode[idx] = mode;
        motor_target[idx] = target;
        motor_new_command_flags[idx / 8] |= (1U << (idx % 8));
    }
}

uint16_t motors_control_get_cycle_of_last_command(int8_t idx) {
    if (idx < MOTORS_CONTROL_MOTOR_COUNT) {
        return motor_last_command_cycle[idx];
    } else {
        return 0;
    }
}

ControlMode motors_control_get_mode(int8_t idx) {
    if (idx < MOTORS_CONTROL_MOTOR_COUNT) {
        return motor_mode_last[idx];
    } else {
        return MOTORS_CONTROL_MODE_NONE;
    }
}

int16_t motors_control_get_actual_position(int8_t idx) {
    if (idx < MOTORS_CONTROL_MOTOR_COUNT) {
        return motor_actual_position[idx];
    } else {
        return 0;
    }
}

int16_t motors_control_get_demand_position(int8_t idx) {
    if (idx < MOTORS_CONTROL_MOTOR_COUNT) {
        return motor_demand_position[idx];
    } else {
        return 0;
    }
}

int16_t motors_control_get_demand_pwm(int8_t idx) {
    if (idx < MOTORS_CONTROL_MOTOR_COUNT) {
        return motor_demand_pwm[idx];
    } else {
        return 0;
    }
}

void motors_control_execute(void) {
    TMR4IE = 0;
    int i;
    for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
        isr_motor_mode[i] = motor_mode[i];
        isr_motor_target[i] = motor_target[i];
    }
    for (i = 0; i < 1 + (MOTORS_CONTROL_MOTOR_COUNT / 8); i++) {
        isr_motor_new_command_flags[i] |= motor_new_command_flags[i];
    }
    TMR4IE = 1;
    for (i = 0; i < 1 + (MOTORS_CONTROL_MOTOR_COUNT / 8); i++) {
        motor_new_command_flags[i] = 0;
    }
}

uint16_t motors_control_get_isr_event_couter(void) {
    static uint16_t motors_control_isr_event_counter_copy = 0;
    if (motors_control_isr_event_updated)
    {
        TMR4IE = 0;
        motors_control_isr_event_counter_copy = motors_control_isr_event_counter;
        motors_control_isr_event_updated = 0;
        TMR4IE = 1;
    }
    return motors_control_isr_event_counter_copy;
}

uint16_t motors_control_feedback(void) {
    TMR4IE = 0;
    uint16_t motors_control_isr_event_counter_copy = motors_control_isr_event_counter;
    int i;
    for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
        motor_actual_position[i] = isr_motor_actual_position[i];
        motor_demand_position[i] = isr_motor_demand_position[i];
        motor_demand_pwm[i] = isr_motor_demand_pwm[i];
        motor_mode_last[i] = isr_motor_mode_last[i];
        motor_last_command_cycle[i] = isr_motor_last_command_cycle[i];
    }
    TMR4IE = 1;
    return motors_control_isr_event_counter_copy;
}

void motors_control_isr(void) {
    if (TMR4IF) {
        TMR4IF = 0;
        //led_on();
        int8_t i = 0;
        for (i = 0; i < MOTORS_CONTROL_MOTOR_COUNT; i++) {
            uint8_t mask = 1U << (i % 8);
            if (isr_motor_new_command_flags[i / 8] & mask) {
                isr_motor_new_command_flags[i / 8] ^= mask;
                isr_motor_last_command_cycle[i] = motors_control_isr_event_counter;
                if (MOTORS_CONTROL_BIT_PST & isr_motor_mode[i]) {
                    motors_control_position_preset[i](isr_motor_target[i]);
                }
                if (MOTORS_CONTROL_MODE_POS == (0x7U & isr_motor_mode[i])) {
                    if (MOTORS_CONTROL_BIT_INC & isr_motor_mode[i]) {
                        isr_motor_demand_position[i] += isr_motor_target[i];
                    } else {
                        isr_motor_demand_position[i] = isr_motor_target[i];
                    }
                } else if (MOTORS_CONTROL_MODE_PWM == (0x7U & isr_motor_mode[i])) {
                    isr_motor_demand_pwm[i] = isr_motor_target[i];
                }
            }
            isr_motor_actual_position[i] = motors_control_position_sensor[i]();
            if (MOTORS_CONTROL_BIT_POS & isr_motor_mode[i]) {
                isr_motor_demand_pwm[i] = servo_task(&position_loop[i], isr_motor_demand_position[i], isr_motor_actual_position[i]);
            } else {
                isr_motor_demand_position[i] = isr_motor_actual_position[i];
            }
            if (MOTORS_CONTROL_BIT_PWM & isr_motor_mode[i]) {
                if (isr_motor_demand_pwm[i] >= 0) {
                    motors_control_L298_forward[i](isr_motor_demand_pwm[i]);
                } else {
                    motors_control_L298_reverse[i](-isr_motor_demand_pwm[i]);
                }
            } else {
                if (MOTORS_CONTROL_BIT_BRK & isr_motor_mode[i]) {

                    motors_control_L298_brake[i]((isr_motor_target[i] < 0) ? -isr_motor_target[i] : isr_motor_target[i]);
                }
                isr_motor_demand_pwm[i] = 0;
            }
            isr_motor_mode_last[i] = isr_motor_mode[i];
        }
        motors_control_isr_event_counter++;
        motors_control_isr_event_updated = 1;
        //led_off();
    }
}
